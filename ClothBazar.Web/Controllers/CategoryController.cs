﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClothBazar.Entities;
using ClothBazar.Services;


namespace ClothBazar.Web.Controllers
{
    public class CategoryController : Controller
    {



        CategoriesService CategoriesService = new CategoriesService();


        // GET: Category
        [HttpGet]
        public ActionResult Index()
        {
            var categories = CategoriesService.GetCategories();
            return View(categories);
        }
       [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Category Category)
        {
            CategoriesService.SaveCategory(Category);
            return View();
        }



    }
}